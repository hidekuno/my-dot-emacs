;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 日本語表示の設定
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; 日本語 info が文字化けしないように

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(auto-compression-mode t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; キーの設定
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-set-key [delete] 'delete-char)
(global-set-key "\C-h" 'backward-delete-char)
(global-set-key "\C-cc" 'compile)
(global-set-key "\C-cb" 'browse-url)
(global-set-key "\C-cg" 'goto-line)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; cc-modeの設定
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun my-c-mode-common-hook ()
;   (c-set-style "linux") (setq indent-tabs-mode t) ;linux 式がいいとき
;      /usr/src/linux/Documentation/CodingStyle 参照
;   (c-set-style "k&r") ;k&r式がいいときはこれを有効にする
;   (c-set-style "gnu") ;デフォルトの設定
  (c-set-style "cc-mode")
  (setq c-tab-always-indent t)
  (setq c-argdecl-indent 4)
  (setq c-continued-brace-offset -4)
  (setq c-continued-statement-offset 4)
  (setq c-indent-level 4)
  (setq c-indent-level 4)
  (setq-default tab-width 4)
  (setq indent-tabs-mode t)
)
(add-hook 'c-mode-common-hook 'my-c-mode-common-hook)

;;GNU 系の Java のコードを編集する
;(defun gnu-mode ()
;  (c-set-style "GNU")
;  (c-set-offset 'inline-open 0)
;)
;(if (fboundp 'jde-mode)
;    (add-hook 'jde-mode-hook 'gnu-mode)
;    (add-hook 'java-mode-hook 'gnu-mode)
(setq c-default-style "k&r")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; その他の言語を設定
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; いろいろ
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;ミニバッファ拡大抑止
(setq resize-mini-windows nil)

;;置換をリージョン指定可能にしたい
(global-set-key "\C-ch" 'thankyou)
(defun thankyou()
  (interactive)
  (cond ((eq nil transient-mark-mode)
         (transient-mark-mode t)
         (when transient-mark-mode
           (defadvice query-replace (around ys:qr-region)
             (if mark-active
                 (save-restriction
                   (narrow-to-region (region-beginning) (region-end))
                   ad-do-return)
               ad-do-return))
           (defadvice query-replace-regexp (around ys:qrr-region)
             (if mark-active
                 (save-restriction
                   (narrow-to-region (region-beginning) (region-end))
                   ad-do-return)
               ad-do-return))
           (ad-enable-advice 'query-replace 'around 'ys:qr-region)
           (ad-enable-advice 'query-replace-regexp 'around 'ys:qrr-region)))
        ((eq t transient-mark-mode)
         (transient-mark-mode nil))))

;(setq ns-command-modifier (quote meta))
;(setq ns-alternate-modifier (quote super))
(define-key global-map [?¥] [?\\])

(custom-set-faces
 '(default ((t
             (:background "black" :foreground "white")
             ))))
 '(cursor ((((class color)
             (background dark))
            (:background "#00AA00"))
           (((class color)
             (background light))
            (:background "#999999"))
           (t ())
           ))
(set-frame-parameter nil 'alhpa 80)


(blink-cursor-mode 0)
(setq cursor-in-non-selected-windows nil)
(setq make-backup-files nil )
(setq scroll-step 1)
(setq fill-column 80)
(setq text-mode-hook 'turn-on-auto-fill)
(setq next-line-add-newlines nil)
(setq column-number-mode t)
(setq line-number-mode t)

(setq inhibit-startup-message t)
(when (eq window-system 'ns)
  (setq exec-path (cons "/opt/local/bin" exec-path)) ;; MacPorts
  (create-fontset-from-ascii-font "Menlo-16:weight=normal:slant=normal" nil "menlokakugo")
  (set-fontset-font "fontset-menlokakugo"
                    'unicode
                    (font-spec :family "Hiragino Kaku Gothic Pro" :size 16)
                    nil
                    'append)
  (add-to-list 'default-frame-alist '(font . "fontset-menlokakugo")))
(setq split-width-threshold nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Schemeの設定
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-hook 'scheme-mode-hook '(lambda ()
                               (setq-default indent-tabs-mode nil)))
;(setq scheme-program-name "guile")
(setq scheme-program-name "/usr/local/bin/gosh")
(autoload 'run-scheme "cmuscheme" "Run an inferior Scheme process." t)
(setq cmuscheme-load-hook
      '((lambda () (define-key inferior-scheme-mode-map "\C-c\C-t"
                                         'favorite-cmd))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(set-frame-parameter nil 'alpha 80)

;(load (expand-file-name (concat (getenv "HOME") "/.emacs.d/init")))
(load-file "/Users/hideki/.emacs.d/rust-mode.el")
(setq rust-format-on-save t)
(add-hook 'before-save-hook 'delete-trailing-whitespace)

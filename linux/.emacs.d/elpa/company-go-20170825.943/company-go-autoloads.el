;;; company-go-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (company-go) "company-go" "company-go.el" (23262
;;;;;;  40090 780251 32000))
;;; Generated autoloads from company-go.el

(autoload 'company-go "company-go" "\


\(fn COMMAND &optional ARG &rest IGNORED)" t nil)

;;;***

;;;### (autoloads nil nil ("company-go-pkg.el") (23262 40090 834541
;;;;;;  497000))

;;;***

(provide 'company-go-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; company-go-autoloads.el ends here

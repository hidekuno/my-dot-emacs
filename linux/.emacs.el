;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 日本語環境設定
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setenv "LANG" "ja_JP.UTF-8")
(setenv "LC_ALL" "ja_JP.UTF-8")
;(set-language-environment "Japanese")
(setq bitmap-alterable-charset 'tibetan-1-column)
;(prefer-coding-system 'utf-8-unix)
;(setq default-file-name-coding-system 'utf-8)

(add-hook 'shell-mode-hook
  (lambda ()
    (set-terminal-coding-system 'utf-8)
    (set-keyboard-coding-system 'utf-8)
    (set-buffer-process-coding-system 'utf-8-unix 'utf-8-unix))
    (modify-coding-system-alist 'process ".*sh\\.exe" '(undecided-dos . undecided-unix)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UNIXシェルの設定
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-font-lock-mode t)
(setq explicit-shell-file-name "bash")
(setq shell-file-name "bash")
(setq shell-command-switch "-c")
(setq shell-file-name-chars "~/A-Za-z0-9_^$!#%&{}@'`.,;()-")
(autoload 'ansi-color-for-comint-mode-on "ansi-color"
             "Set `ansi-color-for-comint-mode' to t." t)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; キーの設定
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-set-key [delete] 'delete-char)
(global-set-key "\C-h" 'backward-delete-char)
(global-set-key "\C-cc" 'compile)
(global-set-key "\C-cb" 'browse-url)
(global-set-key "\C-cg" 'goto-line)
(global-set-key [M-kanji] 'ignore)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; いろいろ
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;(w32-select-font)
;;(set-face-font 'default "ＭＳ ゴシック-12")

;(setq default-frame-alist
;      (append (list
;            '(foreground-color . "white")
;	    '(background-color . "black")
;	    '(border-color . "black")
;	    '(mouse-color . "white")
;	    '(cursor-color . "white")
;	    '(width . 80)
;	    '(height . 40)
;	    '(top . 100)
;	    '(left . 100))
;	      default-frame-alist))

;;ミニバッファ拡大抑止
(setq resize-mini-windows nil)

;;置換をリージョン指定可能にしたい
(global-set-key "\C-ch" 'thankyou)
(defun thankyou()
  (interactive)
  (cond ((eq nil transient-mark-mode)
         (transient-mark-mode t)
         (when transient-mark-mode
           (defadvice query-replace (around ys:qr-region)
             (if mark-active
                 (save-restriction
                   (narrow-to-region (region-beginning) (region-end))
                   ad-do-return)
               ad-do-return))
           (defadvice query-replace-regexp (around ys:qrr-region)
             (if mark-active
                 (save-restriction
                   (narrow-to-region (region-beginning) (region-end))
                   ad-do-return)
               ad-do-return))
           (ad-enable-advice 'query-replace 'around 'ys:qr-region)
           (ad-enable-advice 'query-replace-regexp 'around 'ys:qrr-region)))
        ((eq t transient-mark-mode)
         (transient-mark-mode nil))))

(blink-cursor-mode 0)
(setq cursor-in-non-selected-windows nil)
(setq make-backup-files nil)
(setq scroll-step 1)
(setq fill-column 80)
(setq text-mode-hook 'turn-on-auto-fill)
(setq next-line-add-newlines nil)
(setq column-number-mode t)
(setq line-number-mode t)
(setq fill-column 160)

(setq fill-column 300)
(setq text-mode-hook 'turn-off-auto-fill)

(setq split-width-threshold nil)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Perl mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-hook 'perl-mode-hook
          '(lambda ()
             (setq indent-tabs-mode t)
             (setq tab-width 4)
             (setq perl-close-paren-offset -4)
             (setq perl-continued-statement-offset 4)
             (setq perl-indent-level 4)
             (setq perl-indent-parens-as-block t)
             (setq perl-tab-always-indent t)))
(add-hook 'cperl-mode-hook
          '(lambda ()
             (setq indent-tabs-mode t)
             (setq tab-width 4)
             (setq cperl-close-paren-offset -4)
             (setq cperl-continued-statement-offset 4)
             (setq cperl-indent-level 4)
             (setq cperl-indent-parens-as-block t)
             (setq cperl-tab-always-indent t)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; cua mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(cua-mode t)

;; C-cやC-vの乗っ取りを阻止
(setq cua-enable-cua-keys nil)

;C-x r d	delete-rectangle 矩形領域を削除する
;C-x r o	open-rectangle	 空白文字で埋まった矩形領域を挿入する
;C-x r c	clear-rectangle	 矩形領域を空白文字に置き換える
;C-x r t	string-rectangle 矩形領域を文字列で置き換える
;C-x r k	kill-rectangle	 矩形領域を削除してキルリングに追加
;C-x r y	yank-rectangle	 キルリングの矩形領域を貼り付ける
;C-x r r	copy-rectangle-to-register	レジスターに矩形領域を登録する
;C-x r i	insert-register	 レジスターに登録された矩形領域を貼り付ける

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; その他tips
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; バッファ画面外文字の切り詰め表示
;;(setq truncate-lines nil)

;; ウィンドウ縦分割時のバッファ画面外文字の切り詰め表示
;;(setq truncate-partial-width-windows t)

;; バッファ中の行番号表示
;;(global-linum-mode t)
;;(linum-mode)

;; 変更ファイルのバックアップ
(setq make-backup-files nil)
(setq auto-save-default nil)

;; 編集中ファイルのバックアップ
(setq auto-save-list-file-name nil)
(setq auto-save-list-file-prefix nil)

;;透過モード
;(set-frame-parameter nil 'alpha 75)
;(put 'narrow-to-region 'disabled nil)

;;オートセーブOff
;;(auto-save-mode)

;(if (file-exists-p "/usr/local/bin/lgrep")
;(setq grep-command "lgrep -Au8 -Ia -n")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; cc-modeの設定
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun my-c-mode-common-hook ()
;   (c-set-style "linux") (setq indent-tabs-mode t) ;linux 式がいいとき
;      /usr/src/linux/Documentation/CodingStyle 参照
;   (c-set-style "k&r") ;k&r式がいいときはこれを有効にする
;   (c-set-style "gnu") ;デフォルトの設定
  (c-set-style "cc-mode")
  (setq c-tab-always-indent nil)
  (setq c-argdecl-indent 4)
  (setq c-continued-brace-offset -4)
  (setq c-continued-statement-offset 4)
  (setq c-indent-level 4)
  (setq c-indent-level 4)
  (setq-default tab-width 4)
  (setq indent-tabs-mode nil)
  ;(setq indent-tabs-mode t)
)
(add-hook 'c-mode-common-hook 'my-c-mode-common-hook)

;;GNU 系の Java のコードを編集する
;(defun gnu-mode ()
;  (c-set-style "GNU")
;  (c-set-offset 'inline-open 0)
;)
;(if (fboundp 'jde-mode)
;    (add-hook 'jde-mode-hook 'gnu-mode)
;    (add-hook 'java-mode-hook 'gnu-mode)
(setq c-default-style "k&r")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; クリップボード
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(cond (window-system
       (setq x-select-enable-clipboard t)
       ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; python shell
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;(add-to-list 'load-path "~/.emacs.d")
;(require 'python)
;(setq python-python-command "c:/Python27/python.exe -i")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Schemeの設定
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-hook 'scheme-mode-hook '(lambda ()
                               (setq-default indent-tabs-mode nil)))
(setq scheme-program-name "gosh -i")
;(setq scheme-program-name "~/bin/lisp")
;(setq scheme-program-name "~/rust-elisp/elisp/target/release/lisp")

(autoload 'run-scheme "cmuscheme" "Run an inferior Scheme process." t)
(setq cmuscheme-load-hook
      '((lambda () (define-key inferior-scheme-mode-map "\C-c\C-t"
		      'favorite-cmd))))

;(setq browse-url-browser-function 'w3m-browse-url)
;(add-to-list 'load-path "~/.emacs.d/emacs-w3m-1.4.4/")
;(require 'w3m-load)

(when window-system
(tool-bar-mode -1)
(deftheme molokai
  "Molokai color theme")

(custom-theme-set-faces
 'molokai
 ;; 背景・文字・カーソル
 '(cursor ((t (:foreground "#F8F8F0"))))
 '(default ((t (:background "#1B1D1E" :foreground "#F8F8F2"))))

 ;; 選択範囲
;; '(region ((t (:background "#FE3D3D"))))
 '(region ((t (:background "#0153df"))))

 ;; モードライン
 '(mode-line ((t (:foreground "#F8F8F2" :background "#000000"
                  :box (:line-width 1 :color "#000000" :style released-button)))))
 '(mode-line-buffer-id ((t (:foreground nil :background nil))))
 '(mode-line-inactive ((t (:foreground "#BCBCBC" :background "#333333"
                           :box (:line-width 1 :color "#333333")))))

 ;; ハイライト
 '(highlight ((t (:foreground "#000000" :background "#C4BE89"))))
 '(hl-line ((t (:background "#293739"))))

 ;; 関数名
 '(font-lock-function-name-face ((t (:foreground "#FFFFFF"))))

 ;; 変数名・変数の内容
 '(font-lock-variable-name-face ((t (:foreground "#FFFFFF"))))
 '(font-lock-string-face ((t (:foreground "#E6DB74"))))

 ;; 特定キーワード
 '(font-lock-keyword-face ((t (:foreground "#00FFFF"))))

 ;; Boolean
 '(font-lock-constant-face((t (:foreground "#AE81BC"))))

 ;; 括弧
 '(show-paren-match-face ((t (:foreground "#1B1D1E" :background "#FD971F"))))
 '(paren-face ((t (:foreground "#A6E22A" :background nil))))

 ;; コメント
 '(font-lock-comment-face ((t (:foreground "#FF8F5D"))))

 ;; CSS
 '(css-selector ((t (:foreground "#66D9EF"))))
 '(css-property ((t (:foreground "#FD971F"))))

 ;; nXML-mode
 ;; タグ名
 '(nxml-element-local-name ((t (:foreground "#F92672"))))
 ;; 属性
 '(nxml-attribute-local-name ((t (:foreground "#66D9EF"))))
 ;; 括弧
 '(nxml-tag-delimiter ((t (:foreground "#A6E22A"))))
 ;; DOCTYPE宣言
 '(nxml-markup-declaration-delimiter ((t (:foreground "#74715D"))))

 ;; dired
 '(dired-directory ((t (:foreground "#A6E22A"))))
 '(dired-symlink ((t (:foreground "#66D9EF"))))

  (set-face-foreground 'minibuffer-prompt "cyan")

 ;; MMM-mode
 '(mmm-default-submode-face ((t (:foreground nil :background "#000000")))))

;;;###autoload
;(when load-file-name
;  (add-to-list 'custom-theme-load-path
;               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'molokai)
;(load-theme 'molokai t)
(enable-theme 'molokai)
)
;========================================================================
; for go mode install
;========================================================================
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)
(package-refresh-contents)


(defun set-exec-path-from-shell-PATH ()
  (let ((path-from-shell (replace-regexp-in-string
                          "[ \t\n]*$"
                          ""
                          (shell-command-to-string "$SHELL --login -i -c 'echo $PATH'"))))
    (setenv "PATH" path-from-shell)
    (setq eshell-path-env path-from-shell) ; for eshell users
    (setq exec-path (split-string path-from-shell path-separator))))

(when window-system (set-exec-path-from-shell-PATH))

(setenv "GOPATH" "/home/kunohi/go")

(add-to-list 'exec-path "/home/kunohi/go/bin")
(add-hook 'before-save-hook 'gofmt-before-save)
(defun my-go-mode-hook ()
  ; Call Gofmt before saving
  (add-hook 'before-save-hook 'gofmt-before-save)
  ; Godef jump key binding
  (local-set-key (kbd "M-.") 'godef-jump)
  (local-set-key (kbd "M-*") 'pop-tag-mark)
  )
(add-hook 'go-mode-hook 'my-go-mode-hook)
(defun auto-complete-for-go ()
  (auto-complete-mode 1))
(add-hook 'go-mode-hook 'auto-complete-for-go)

(let ((default-directory (expand-file-name "~/.emacs.d/elpa")))
  (add-to-list 'load-path default-directory)
  (if (fboundp 'normal-top-level-add-subdirs-to-load-path)
      (normal-top-level-add-subdirs-to-load-path)))

(require 'go-autocomplete)
(require 'auto-complete-config)

(load-file "~/.emacs.d/rust-mode.el")
(setq rust-format-on-save t)
(add-hook 'before-save-hook 'delete-trailing-whitespace)
